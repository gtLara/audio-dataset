# Audio Dataset - PT

Os arquivos deste repositório criam uma base de dados de treinamemento para sistemas voz para texto e texto para voz. Em sistemas neurais, tais dados se apresentam na forma de "pares" - trechos de texto e sua leitura em fala natural. De acordo com as características da base *LJSpeech* a duração dos trechos não deve exceder 10 segundos epossuir em média 6 segundos. Apesar do controle preciso de duração não ser implementado nesse projeto, os áudios resultantes acabam encaixando nessas descrições por serem traços relacionadas à duração de frases em linguagem natural.

# Uso
 
Usando o pacote aeneas para realizar o alinhamento forçado, o processo é realizado pelo arquivo `align.sh` por meio da seguinte sintaxe:

```sh
./align.sh texto.mp3 audio.mp3 saida.json
``` 
