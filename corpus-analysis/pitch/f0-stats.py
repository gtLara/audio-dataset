#Rotina de análise prosódica de áudio (TTS)
import numpy as np
import parselmouth
import os
import time
import pandas as pd

start_time = time.time()

def analyze_stats(apth):
    sound = parselmouth.Sound(apth)
    pitch = sound.to_pitch()
    contour = pitch.selected_array['frequency']
    
    contour[contour == 0] = np.nan
    clean = [c for c in contour if not np.isnan(c)]
    
    mean = np.mean(clean)
    std = np.std(clean)
    
    return {'mean': mean, 'std': std}

pth = "/home/gabriel-lara/ljspeech/LJSpeech-1.1/wavs-16000/"

audio_list = os.listdir(pth)

data_list = [analyze_stats(pth+audio) for audio in audio_list]
    
mean = round(np.mean([data['mean'] for data in data_list]), 2)
std = round(np.mean([data['std'] for data in data_list]), 2)

print("--- %s seconds ---" % (time.time() - start_time))


