import numpy as np 
import parselmouth
import os
import time
from random import sample

stime = time.time()

def pitch(wave):
    
    audiol = parselmouth.Sound(wave)

    pitch = audiol.to_pitch()

    f = pitch.selected_array['frequency']

    if True in np.isnan(f): #teste de NaN
        f = f[~np.isnan(f)]
        
    if 0 in f: #teste de zeros
        f = f[f != 0]
    
    return f 

def plot(audios, n=1):
    for indx in range(n):
        f0 = pitch(sample(audio, 1))
        plt.plot(f0)
        return
        

def adjacent_distance(audio):
    
    f0 = pitch(audio)
            
    adjacent_distance = 0
    
    for p, point in enumerate(f0[1:-1]):
        adjacent_distance += abs(point - f0[p + 2])
    
    if len(f0) - 2 == 0:
        adjacent_distance = np.nan
    else:
        adjacent_distance /= len(f0) - 2
    
    return round(adjacent_distance, 2)

def local_curvature(audio): #stall
    
    f0 = pitch(audio)
        
    f0 = f0[:-(len(f0)%3)]
    
    mean_curve = 0
    
    for index in np.arange(0, len(f0), 3):
        points = f0[index:index+3]
        #polinomial = interpolate(points)
        #curve = curvature(polinomial)
        mean_curve += points
    
    mean_curve /= len(np.arange(0, len(f0), 3))
    
    return mean_curve

def density(audio):
    
    f0 = pitch(audio)
    
    plt.hist(f0)
    
    return f0
    
pth = "/home/gabriel-lara/ljspeech/LJSpeech-1.1/wavs-16000/"

audio_list = [pth+name for name in os.listdir(pth)]

adjacent_list = [adjacent_distance(audio) for audio in audio_list]

adjacent_mean = round(np.mean([value for value in adjacent_list if not np.isnan(value)]), 2)

print(time.time() - stime)

#FALTA: 
#- FAZER ADJ DISTANCE PARA LUSIADAS 
#- FAZER INTERPOLAÇÃO