from sklearn.feature_extraction.text import CountVectorizer
import math

def compute(text_path, visual = True, simple = False):
    with open(text_path, 'r') as tf:
        text = tf.read()
    
    t_text = text.split("\n")
    vectorizer = CountVectorizer()
    vectorizer.fit_transform(t_text)
        
    vocabulary = vectorizer.vocabulary_
    
    V = len(vocabulary)
    N = len(text.split())
    
    if simple: ttr = V/N
    else: ttr = -math.log(V/N)
    
    if visual: print(ttr)

    