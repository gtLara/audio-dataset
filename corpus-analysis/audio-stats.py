import numpy as np 
import os 
import json 
import pandas as pd
import sys 
from nltk.corpus import stopwords
from pydub import AudioSegment

def raw_read(pth):
    
    df = pd.read_csv(pth+"metadata.csv", sep = "|", header = None)
    
    text_list = list(df[1])
    
    durations = [len(AudioSegment.from_file(pth+"wavs-16000/"+audio))/1000 for audio in os.listdir(pth+"wavs-16000/")]
    
    return text_list, durations

STOPWORDS = stopwords.words('portuguese')

durations = []
text_list = []

pth = "/home/gabriel-lara/jsons/" #caminho para pasta de jsons

for file in os.listdir(pth):
    with open(pth + file) as f:
        data = json.load(f)
        
    duration_list = [[]]
    
    for jn, j in enumerate(data['fragments']):
        tl = []
        
        tl.append(j['begin'])
        tl.append(j['end'])
        
        diff = float(tl[1]) - float(tl[0])
        
        txt = j['lines'][0]
        
        if(diff > 0.3 and txt != ""): 
            durations.append(diff)
            text_list.append(txt)

char_count_list = [len(text) for text in text_list]
text_list = [text.split() for text in text_list]
word_count_list = [len(text) for text in text_list]
clean_word_count_list = [len(data) for data in 
                         [[word for word in [t.replace(",", "") for t in text] 
                         if word not in STOPWORDS] 
                         for text in text_list]]

char_per_second_list = []
word_per_second_list = []

for t, time in enumerate(durations):
    char_per_second_list.append(char_count_list[t]/time)
    word_per_second_list.append(word_count_list[t]/time)
    
cps_mean = np.mean(char_per_second_list)
cps_std = np.std(char_per_second_list)
wps_mean = np.mean(word_per_second_list)
wps_std = np.std(word_per_second_list)

mean = np.mean(durations)
std_dev = np.std(durations)
mx = np.max(durations)
mn = np.min(durations)
total = np.sum(durations)
clip_number = len(durations)

print(cps_mean)

print(cps_std)

time_list = [0, 0, 0] #hours, minutes, seconds

while total > 60:
    total -= 60
    time_list[1] += 1

time_list[2] = total 

while time_list[1] > 60:
    time_list[1] -= 60
    time_list[0] += 1

total_words = sum(word_count_list)
total_clean_words = sum(clean_word_count_list)
vocab = set()
clean_vocab = set()

time_list = [round(time, 2) for time in time_list]

for txt in text_list:
    for word in txt:
        vocab.add(word)
        if word not in STOPWORDS:
            clean_vocab.add(word)

unique_words = len(vocab)
unique_clean_words = len(clean_vocab)

simple_ttr = unique_words/total_words
clean_ttr = unique_clean_words/total_clean_words

mean_words = total_words/clip_number
total_dur= str(time_list[0]) + ":" + str(time_list[1]) + ":" + str(time_list[2])

data_list = [clip_number, mn, mx, mean, std_dev, -1, total_words, mean_words, 
             cps_mean, wps_mean, cps_std, wps_std, unique_words, simple_ttr, clean_ttr]

data_list = [round(data, 2) for data in data_list]


data_list[data_list.index(-1)] = total_dur

names = ['Número de Clipes','Duração Mínima', 
                           'Duração Máxima', 'Duração Média', 
                           'Desvio Padrão de Duração', 'Duração Total',
                           'Palavras Totais', 'Média de Palavras por Clipe',
                           'Média de Caracteres por Segundo(CPS)',
                           'Média de Palavras por Segundo(PPS)',
                           'Desvio padrão de Média de CPS por Clipe',
                           'Desvio padrão de Média de PPS por Clipe',
                           'Palavras Distintas', 'TTR Simples', 'TTR Líquido']

intermediate = {'Stats': names,
                'Podcast': data_list}

#df = pd.DataFrame(intermediate)
#
#df.to_csv("lj-stats.csv", index = False)