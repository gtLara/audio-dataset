 import os
import pandas as pd
import numpy as np
import math
from pydub import AudioSegment

def get_sorted_files(path, func=''):
    splitted_audios = os.listdir(f'{path}')
    if func == '':
        splitted_audios = sorted(splitted_audios)
    else:
        splitted_audios = sorted(splitted_audios, key=func)
    
    return(splitted_audios)

def sort_pt_br(audio_name):
    return(int(audio_name.split('pt_br')[1]))

## PATHS
processed_data_path = "./processed_wavs"
input_audio_path = "./trim-1.wav"
map_path = "./map-10.csv"

## LOAD METADATA
data = pd.read_csv(map_path)
data.columns = ["id", "start", "end", "phrase"]

## LOAD FULL AUDIO
input_audio = AudioSegment.from_wav(input_audio_path)

## SPLIT AUDIO ACCORDINGLY TO METADATA
if not os.path.exists(processed_data_path):
    os.mkdir(processed_data_path)
    
for i in range(data.shape[0]):
    t0 = data['start'][i] * 1000
    t1 = data['end'][i]   * 1000
    
    splitted_audio = input_audio[t0:t1]
    splitted_audio.export(f'{processed_data_path}/{data["id"][i]}.wav', format="wav")
    
## REMOVE EMPTY AUDIO FILES
nan_idxs = [idx for idx, phrase in enumerate(data['phrase']) if not isinstance(phrase, str)]
nan_files_names = list(data.loc[nan_idxs, "id"])

for nan_file in nan_files_names:
    try:
        audio_path = f'{processed_data_path}/{nan_file}.wav'
        os.remove(audio_path)
    except:
        next

splitted_audios = get_sorted_files(processed_data_path)
for audio_file in splitted_audios:
    audio_path = f'{processed_data_path}/{audio_file}'
    try:
        splitted_audio = AudioSegment.from_wav(audio_path)
        audio_power = splitted_audio.dBFS
        if audio_power < -30:
            os.remove(audio_path)
    except:
        os.remove(audio_path)

data = data.drop(nan_idxs)
data = data.reset_index(drop=True)

## RENAME REMAINING AUDIO FILES
splitted_audios = get_sorted_files(processed_data_path)
max_number = int(max(splitted_audios).split("f")[1].split(".wav")[0])
n_audios = len(splitted_audios)

for i, audio_file in enumerate(splitted_audios):
    audio_path = f'{processed_data_path}/{audio_file}'
    os.rename(audio_path, f'{processed_data_path}/pt_br{n_audios+i}')

splitted_audios = get_sorted_files(processed_data_path)
              
for i, audio_file in enumerate(splitted_audios):
    audio_path = f'{processed_data_path}/{audio_file}'
    os.rename(audio_path, f'{processed_data_path}/pt_br{i}')
    

splitted_audios = get_sorted_files(processed_data_path, sort_pt_br)
final_data = [f'{audio_name}|{data.loc[i, "phrase"]}' for i, audio_name in enumerate(splitted_audios)]
final_df = pd.DataFrame(final_data)

final_df = pd.DataFrame(final_data)
final_df.to_csv('mdata.csv', index=False)