#!/bin/bash
#The functionality of this script relies upon some assumptions about the audio input. They are listed below:

# 1: The input is a mp3 file
# 2: The input runtime is under 10 minutes
# 3: The hearder or "trash" duration is under 10 seconds

#Observations:

#Due to an error not patched until the writing of this script the use of the sox trimming function on line 41/43 throws a warning
#which might be misleading. To correct this, all error messages have been supressed as per standard use.

pre="trim-"

full=$1
ext=".wav"
stem=${full%.mp3}
stem=${stem#./audio/}
aux="$stem$ext"

out="$pre$aux"
trash=$2

size=$(mp3info -p "%S" $1)
dsize=$size

mpg123 -q -w $aux $1

minutes=0
seconds=0

while [ $dsize -gt 60 ] 
do
	(( dsize -= 60 ))
	((minutes++))
done

if [ "$dsize" -eq 0 ] 
then
	seconds=59
	((minutes--))
else
	((dsize -= 1))
	seconds=$dsize
fi

if [ "$seconds" -le 10 ]
then
	sox $aux $out trim 00:00:0$trash 00:0$minutes:0$seconds > /dev/null
else
	sox $aux $out trim 00:00:0$trash 00:0$minutes:$seconds > /dev/null
fi

rm $aux
