#!/bin/bash

path=$1
trash=$2

for f in $path
do
	bash .subtrim.sh "$f" $trash 2> /dev/null
done

mkdir trimmed
mv trim-* trimmed
