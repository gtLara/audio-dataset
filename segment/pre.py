#args: JSON, audio, stem
import json 
import sys
import os
import pandas as pd
import numpy as np 
from pydub import AudioSegment 

def cut(jsp, audiop, cn):
    
    with open(jsp) as f:
        data = json.load(f)
        
    audio = AudioSegment.from_file(audiop)
    
    duration_list = [[]]
    txt_list = [[]]
    
    for jn, j in enumerate(data['fragments']):
        
        tl = []
        
        tl.append(j['begin'])
        tl.append(j['end'])
        
        diff = float(tl[1]) - float(tl[0])
        
        tl = [float(dur)*1000 for dur in tl]
        
        line = j['lines']
            
        if(line[0] != "" and diff > 0.3):
            duration_list.append(tl)
            txt_list.append(line)
        
    del duration_list[0], txt_list[0]
    
    audio_list = []
    
    for duration in duration_list:
        segment = audio[duration[0]:duration[1]]
        audio_list.append(segment)
    
    audio_path = "./wavs"
    
    file_names = [str(cn) + '-' + str(n) + '.wav' for n in np.arange(1, len(audio_list) + 1)]
        
    for a, audio in enumerate(audio_list):
        audio.export(audio_path + file_names[a], format = "wav")
    
    if not (os.path.exists("metadata.csv")):
        df = pd.DataFrame(columns = ['filename', 'text'])
        df['filename'] = file_names
        df['text'] = txt_list = [txt[0] for txt in txt_list]
        df.to_csv("metadata.csv", sep = '|', index = False)
    else:
        metadata = pd.read_csv("metadata.csv", index_col=0, sep = '|')
        df = pd.DataFrame(columns = ['filename', 'text'])
        df['filename'] = file_names
        df['text'] = txt_list = [txt[0] for txt in txt_list]
        metadata = metadata.append(df, ignore_index = True)
        metadata.to_csv("metadata.csv", sep = '|', index = False)

cut(sys.argv[1], sys.argv[2], sys.argv[3])

