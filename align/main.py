 #!/usr/bin/python
 # -*- coding: utf-8 -*-
from nltk import sent_tokenize
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

with open(sys.argv[1], 'r') as f:
    data = f.read()

paragraphs = data.split('\n\n')

t_paragraphs = []

for p in paragraphs:
    p = p.replace("\n", " ")
    p = p.replace("'", "")
    t_paragraphs.append(sent_tokenize(p))

p_text = open(sys.argv[2], "w+")

for paragraph in t_paragraphs:
    p_text.write("\n\n")
    for sentence in paragraph:
        p_text.write(sentence)
        p_text.write("\n")
        
p_text.close()